import os
from enum import Enum


class ModeToVerb(Enum):
    """request sending method"""
    GET = "ОТДОВАЙ"
    REMOVE = "УДОЛИ"
    POST = "ЗОПИШИ"
    BLESS_ME = "АМОЖНА?"


class Statuses(Enum):
    """statuses from the check server"""
    OK = "НОРМАЛДЫКС"
    NOT_FOUND = "НИНАШОЛ"
    NOT_APPROVED = "НИЛЬЗЯ"
    ACCEPT = "МОЖНА"
    INCORRECT_REQUEST = "НИПОНЯЛ"


PROTOCOL = "РКСОК/1.0"

FSO_DOMAIN = 'vragi-vezde.to.digital'
FSO_PORT = 51624

NAME_SALT = "e2286bf8bb3feae099f64b0f3c60d9f66d1687cd939e31ebfd216e3e95c71fcf"

PATH = f"{os.getcwd()}/data/"
