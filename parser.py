import hashlib
from typing import Tuple, Optional
from config import Statuses, PROTOCOL, NAME_SALT


async def pars_client_message(data: bytes, writer) -> Optional[Tuple[str, str, str]]:
    """
    Parse data from the client. Get the "method", "name" and "protocol"
    for data validation and processing.
    """
    try:
        method = data.decode().split()[0]
        name = data.split(b'\r\n')[0].split(PROTOCOL.encode())[0].decode().split(' ', 1)[1]  # sorry
        protocol = data.split(b'\r\n')[0].split(name.encode())[1].decode()
        check_list = [method, name, protocol]
        if len(check_list) < 3:
            writer.write(f"{Statuses.INCORRECT_REQUEST} {PROTOCOL}".encode())
            await writer.drain()
        else:
            return method, name.strip(), protocol
    except IndexError:
        writer.write(f"{Statuses.INCORRECT_REQUEST} {PROTOCOL}".encode())
        await writer.drain()
    writer.close()


async def make_hash_noise(name: str) -> str:
    name_hash = hashlib.sha256((name + NAME_SALT).encode()).hexdigest().lower()
    return name_hash


