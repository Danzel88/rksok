import os
import aiofiles
import aiofiles.os
from parser import make_hash_noise
from config import PROTOCOL, Statuses, PATH


async def write_file(data: bytes, writer, name, response: bytes):
    """
    Writing a file with client input data. Parses the response from the validation server,
    if response status - "МОЖНА": writes a file with name: 'name' and content: 'phone'. Writes the response
    from the verification server to the base socket (returns to the client)
    """
    response_from_check_server = response.decode().split()[0]
    phone_list = [''.join(i.strip().split('\r')) for i in data.decode().split('\n')[1::]
                  if len(i) >= 1]
    phone = ''.join(phone_list)
    if 1 <= len(name) <= 30:
        if response_from_check_server == Statuses.ACCEPT:
            async with aiofiles.open(f"{PATH}{await make_hash_noise(name)}", 'w',) as f:
                await f.writelines(f"{phone}")
            writer.write(f"{Statuses.OK} {PROTOCOL}".encode())
            await writer.drain()
        else:
            writer.write(response)
            await writer.drain()
            # writer.close()
    else:
        answer_for_client = f"{Statuses.INCORRECT_REQUEST} {PROTOCOL}".encode()
        writer.write(answer_for_client)
        await writer.drain()
    writer.close()


async def remove_file(data: bytes, writer, name, response: bytes):
    """
    Deleting a file by name. Parses the response from the validation server,
    if response status - "МОЖНА", and the file with the specified name
    exists: deletes this file. Else it writes the response from
    the validation server to the base socket (returns to the client)
    """
    if response.decode().split()[0] == Statuses.ACCEPT:
        if len(name) < 30:
            path = f"{PATH}{await make_hash_noise(name)}"
            if os.path.exists(path):
                await aiofiles.os.remove(path)
                answer_for_client = f"{Statuses.OK} {PROTOCOL}"
                writer.write(answer_for_client.encode())
                await writer.drain()
            elif not os.path.exists(data.decode().split()[1]):
                answer_for_client = f"{Statuses.NOT_FOUND} {PROTOCOL}"
                writer.write(answer_for_client.encode())
                await writer.drain()
                # writer.close()
        else:
            writer.write(response)
            await writer.drain()
            # writer.close()
    else:
        writer.write(response)
        await writer.drain()
        # writer.close()
    writer.close()
    await writer.wait_closed()


async def read_file(writer, name: str, response: bytes):
    """
    Search for a file by the specified name. Parses the response from the validation server,
    if response status - "МОЖНА", and the file with the specified name exists: writes the
    response from the verification server and the contents of the found file
    to the base socket (returns to the client)
    """
    if response.decode().split()[0] == Statuses.ACCEPT:
        if 1 <= len(name) <= 30:
            if os.path.exists(f"{PATH}{await make_hash_noise(name)}"):
                async with aiofiles.open(f"{PATH}{await make_hash_noise(name)}") as f:
                    phone = await f.read()
                writer.write(f"{Statuses.OK} {PROTOCOL}\r\n{phone}".encode())
                await writer.drain()
            else:
                writer.write(f"{Statuses.NOT_FOUND} {PROTOCOL}".encode())
                await writer.drain()
        else:
            writer.write(f"{Statuses.INCORRECT_REQUEST} {PROTOCOL}".encode())
            await writer.drain()
    else:
        writer.write(response)
        await writer.drain()
    writer.close()
