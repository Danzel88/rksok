import asyncio
from parser import pars_client_message
from file_handlers import write_file, remove_file, read_file
from config import ModeToVerb, FSO_PORT, FSO_DOMAIN, PROTOCOL, Statuses


async def requests_handler(reader, writer):
    """
    Main handler of requests and responses. Prepares and sends a request to
    the verification server. The received response, user input,
    and client socket are passed to file_handlers.
    """
    data = await reader.read(1024)
    check_reader, check_writer = await asyncio.open_connection(FSO_DOMAIN, FSO_PORT)
    request_for_check_server = f"{ModeToVerb.BLESS_ME} " \
                               f"{PROTOCOL}\r\n".encode() + data
    check_writer.write(request_for_check_server)
    await check_writer.drain()
    response_check_server = await check_reader.read(1024)
    if await pars_client_message(data, writer):
        method, name, protocol = await pars_client_message(data, writer)
        if method == ModeToVerb["GET"] and protocol == PROTOCOL:
            await read_file(writer, name, response_check_server)
        elif method == ModeToVerb["POST"] and protocol == PROTOCOL:
            await write_file(data, writer, name, response_check_server)
        elif method == ModeToVerb["REMOVE"] and protocol == PROTOCOL:
            await remove_file(data, writer, name, response_check_server)
        else:
            writer.write(f"{Statuses.INCORRECT_REQUEST} {PROTOCOL}".encode())
            await writer.drain()
            # writer.close()
            # await writer.wait_closed()
    else:
        writer.write(f"{Statuses.INCORRECT_REQUEST} {PROTOCOL}".encode())

    check_writer.close()
    await check_writer.wait_closed()
    writer.close()


async def main():
    server = await asyncio.start_server(requests_handler, '82.148.19.173', 8888)
    # server = await asyncio.start_server(requests_handler, '127.0.0.1', 8888)  #for devhost up
    async with server:
        await server.serve_forever()


if __name__ == '__main__':
    asyncio.run(main())
